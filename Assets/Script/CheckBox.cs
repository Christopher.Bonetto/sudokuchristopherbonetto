﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckBox : MonoBehaviour
{
    // Start is called before the first frame update
    public int currentNumber = -1;
        
    private Text buttonText;

    public int currentRow;
    public int currentColumn;

    private int probability;
    private int randomNumber;
    private bool haveNumber;

    // Start is called before the first frame update
    void Awake()
    {
        buttonText = GetComponentInChildren<Text>();
        buttonText.text = "";

        probability = Random.Range(0, 100);
        if (probability < 20)
        {
            haveNumber = true;
            randomNumber = Random.Range(1, 10);
            GameManager.instance.SudokuGrid[currentRow, currentColumn] = randomNumber;
            buttonText.text = randomNumber.ToString();

        }
    }

    private void Start()
    {
        
        do
        {
            if (haveNumber)
            {
                randomNumber = Random.Range(1, 10);
                GameManager.instance.SudokuGrid[currentRow, currentColumn] = randomNumber;
                buttonText.text = randomNumber.ToString();
            }
            

        } while (!GameManager.instance.CheckPosition(currentRow, currentColumn, randomNumber));


    }



    public void ChangeNumberOnClick()
    {
        currentNumber = GameManager.instance.OnClickNumber;
        
        buttonText.text = currentNumber.ToString();
        GameManager.instance.SudokuGrid[currentRow, currentColumn] = currentNumber;
        if(GameManager.instance.CheckPosition(currentRow, currentColumn, currentNumber)==false)
        {
            buttonText.color = Color.red;
        }
        else
        {
            buttonText.color = Color.black;
        }
    }
}
