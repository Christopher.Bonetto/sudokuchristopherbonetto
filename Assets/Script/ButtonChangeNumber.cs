﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonChangeNumber : MonoBehaviour
{
    private int number;
    private Text numberText;

    // Start is called before the first frame update

    private void Awake()
    {
        numberText = GetComponentInChildren<Text>();
    }

    void Start()
    {
        
        number = int.Parse(gameObject.name);
        numberText.text = number.ToString();
    }

    public void ButtonClick()
    {
        GameManager.instance.OnClickNumber = number;
        
    }
}
