﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    
    public GameObject boxPrefab;
    public GameObject GridSpawn;

    public int OnClickNumber = -1;
    Text numberText;

    public int[,] SudokuGrid = new int[9,9];

    private int probability;
    private int randomNumber;
    Text randomText;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        
    }

    private void Start()
    {
        GridGeneration();
        
    }

    public void GridGeneration()
    {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                GameObject button = Instantiate(boxPrefab, GridSpawn.transform);
                button.name = i + " " + j;

                CheckBox myButton = button.GetComponent<CheckBox>();
                SudokuGrid[i, j] = myButton.currentNumber;
                myButton.currentRow = i;
                myButton.currentColumn = j;
            }  
        }
    }




    public bool CheckRow(int row, int currentNumber)
    {
        int cnt = 0;
        for (int i = 0; i < 9; i++)
        {
            if (currentNumber == SudokuGrid[row, i])
                cnt++;
            
        }
        
        return cnt <= 1;
    }

    public bool CheckColumn(int column, int currentNumber)
    {
        int cnt = 0;
        for (int i = 0; i < 9; i++)
        {
            if (currentNumber == SudokuGrid[i, column])
                cnt++;
        }

        return cnt <= 1;
    }

    public bool CheckCurrentTris(int row, int colunm, int currentNumber)
    {
        int cnt = 0;

        return cnt <= 1;
    }


    public bool CheckPosition(int row, int colunm, int currentNumber)
    {
        if (CheckRow(row, currentNumber))
            if (CheckColumn(colunm, currentNumber))
                if (CheckCurrentTris(row, colunm, currentNumber))
                    return true;

        return false;
    }
}
